﻿using SealReportDemo.DAL;
using SealReportDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealReportDemo.BAL
{
    public class SealReportBAL
    {
        SealReportDAL krd = new SealReportDAL(Util.getConnStr());

        public SealReportBAL() { }

        public void InsertSealReportConfig(SealReportModels sealReportModels)
        {
            krd.InsertSealReportConfig(sealReportModels);
        }

        public string GetSealReportConfigByFileName(string filename)
        {
           return krd.GetSealReportConfigByFileName(filename);
        }
    }
}
