﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealReportDemo.Models
{
    public class SealReportModels
    {
        public int SealReportConfigId { get; set; }

        public String ReportName { get; set; }

        public String ReportPath { get; set; }

    }
}
