﻿using Seal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static SealReportDemo.RepositoryInitialise;

namespace SealReportDemo
{
    public class SingletonService : IRepository
    {
        Repository repository ;
        public SingletonService()
        {
            repository = Repository.Create();
        }

       public Repository Initialize()
        {
            return repository;
        }
    }
}
