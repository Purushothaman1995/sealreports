﻿using Dapper;
using SealReportDemo.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SealReportDemo.DAL
{
    public class SealReportDAL
    {
        private String sConnectionString;

        String inSQL = string.Empty;

        public SealReportDAL(String ConnectionString)
        {
            sConnectionString = ConnectionString;
        }
        public async void InsertSealReportConfig(SealReportModels sealReportModels)
        {
            using (IDbConnection db = new SqlConnection(sConnectionString))
            {

                inSQL = @"INSERT INTO SealReportConfig(ReportName,ReportPath) VALUES (@ReportName,@ReportPath)";
                var p = new DynamicParameters();
                p.Add("ReportName", sealReportModels.ReportName, DbType.String);
                p.Add("ReportPath", sealReportModels.ReportPath, DbType.String);
                await db.ExecuteAsync(inSQL, p, commandType: CommandType.Text);
            }
        }
        public String GetSealReportConfigByFileName(string fileName)
        {
            using (IDbConnection dbConnection = new SqlConnection(sConnectionString))
            {

                string sQuery0 = "SELECT ReportPath FROM SealReportConfig where ReportName = @fileName";
                dbConnection.Open();
                return dbConnection.QueryFirstOrDefault<String>(sQuery0, new { fileName = fileName });
            }
        }
    }
}
