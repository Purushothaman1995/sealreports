﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Seal.Model;
using SealReportDemo.BAL;
using SealReportDemo.DAL;
using SealReportDemo.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static SealReportDemo.RepositoryInitialise;

namespace SealReportDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SealReportController : ControllerBase
    {
        SealReportBAL krd = new SealReportBAL();
        private readonly IRepository _singletonService1;
        Repository repository;
        public SealReportController(IRepository repository)
        {
            _singletonService1 = repository;
        }
      
        [HttpGet]
            public ContentResult ListReportByName(string reportName)
            {
             var filepath = krd.GetSealReportConfigByFileName(reportName);
             repository = _singletonService1.Initialize();
             Report report = Report.LoadFromFile(filepath, repository);
             ReportExecution execution = new ReportExecution() { Report = report };
             execution.Execute();
             while (report.Status != ReportStatus.Executed) System.Threading.Thread.Sleep(100);
             return new ContentResult
                {
                    ContentType = "text/html",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = execution.GenerateHTMLResult()
                 };
            }
    }
}
