﻿using Seal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealReportDemo
{
    public class RepositoryInitialise
    {
        public interface IRepository
        {
            Repository Initialize();
        }     
    }
}
